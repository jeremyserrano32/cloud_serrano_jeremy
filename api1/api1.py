from flask import Flask, request
import requests

app = Flask(__name__)

@app.route('/api1', methods=['GET'])
def api1():
    response = requests.get('http://api2-service/api2')
    return response.json()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
