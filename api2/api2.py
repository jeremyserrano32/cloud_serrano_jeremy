from flask import Flask

app = Flask(__name__)

@app.route('/api2', methods=['GET'])
def api2():
    return {"message": "Je mérite de valider le module"}

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
